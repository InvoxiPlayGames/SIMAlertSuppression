# SIM Alert Suppression
Suppresses No SIM/Locked SIM/Bad SIM alerts that happen on respring on iOS 5 and above. Useful for iPhones used as iPod touches.
